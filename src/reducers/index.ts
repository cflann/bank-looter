import { combineReducers } from 'redux'

import appReducer from './appSlice'
import driveReducer from './driveSlice'
import samplesReducer from './samplesSlice'

export default combineReducers({
  app: appReducer,
  drive: driveReducer,
  samples: samplesReducer
})
