import { ipcRenderer } from 'electron'
import { createSlice } from '@reduxjs/toolkit'
import { Dispatch } from 'redux'
import range from 'lodash/range'

import { AppThunk } from '../store'
import { Sample } from 'sp404-tools'

const indexFromLineNo = (lineNo: number): [ string, number ]  => {
  const bankNo = Math.floor(lineNo / 12) // 0 - 10                                                    
  const bankIndex = String.fromCharCode(bankNo + 65) // A - J
  const padIndex = lineNo % 12 + 1 // 1 - 12
  return [ bankIndex, padIndex ]
}

const getEmptyState = () => range(120).map((i) => {
  const [ bankIndex, padIndex ] = indexFromLineNo(i)
  return { bankIndex, padIndex, sourcePath: '' } as Sample
})

const samplesSlice = createSlice({
  name: 'samples',
  initialState: getEmptyState() as Sample[],
  reducers: {
    clearSample(state, action) {
      const absIndex = action.payload.index
      const { bankIndex, padIndex } = state[absIndex]
      state[absIndex] = { bankIndex, padIndex, sourcePath: '' } as Sample // empty sample
    },
    loadSample(state, action) {
      const { index: absIndex, filePath } = action.payload
      const { bankIndex, padIndex } = state[absIndex]
      state[absIndex] = { bankIndex, padIndex, sourcePath: filePath } as Sample
    },
    loadImport(state, action) {
      return action.payload.samples
    }
  }
})

export const { clearSample, loadSample, loadImport } = samplesSlice.actions

export default samplesSlice.reducer

export const selectWaveFile = (absIndex: number): AppThunk => (async (dispatch: Dispatch) => {
  const { canceled, filePaths } = await ipcRenderer.invoke('select-wave-file')
  if (!canceled) {
    dispatch(loadSample({ index: absIndex, filePath: filePaths.pop() }))
  }
})
