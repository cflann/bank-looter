import { createSlice } from '@reduxjs/toolkit'

const appSlice = createSlice({
  name: 'app',
  initialState: {
    activeIndex: -1,
    exporting: false
  },
  reducers: {
    setActiveIndex(state, action) {
      const { index } = action.payload
      state.activeIndex = index
    },
    exportStart(state) {
      state.exporting = true
    },
    exportSuccess(state) {
      console.log('export successful!')
      state.exporting = false
    },
    exportFailure(state, action) {
      console.error(action.payload.error)
      state.exporting = false
    },
  }
})

export const { setActiveIndex, exportStart, exportSuccess, exportFailure } = appSlice.actions

export default appSlice.reducer
