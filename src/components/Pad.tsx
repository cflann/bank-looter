import React from 'react'
import styled from 'styled-components'

import { Sample } from 'sp404-tools'

import PadAction from './PadAction'
import { Colors } from './style'

export const Wrapper = styled.div<{ active?: boolean, loaded?: boolean }>`
  background-color: ${props => props.active 
                      ? Colors.YELLOW 
                      : props.loaded
                        ? Colors.CYAN
                        : 'transparent'};
  border: 1px solid black;
  height: 45px;
  width: 60px;
  position: relative;

  &:hover {
    background-color: ${props => props.active 
                        ? Colors.DARK_YELLOW
                        : props.loaded
                          ? Colors.DARK_CYAN
                          : Colors.GRAY}
  }
`

Wrapper.defaultProps = {
  active: false
}

export interface PadProps {
  active: boolean
  index: number
  waveform?: any // TODO: what will this look like? USE: indicates non-empty, visual representation
  onClick: (index: number) => void
  onClickLoad: (index: number, done: () => void) => void
  onClickRemove: (index: number) => void
  onClickPlay: (index: number, done: () => void) => void
  onClickStop: (index: number) => void
}

interface PadState {
  loading: boolean
  playing: boolean
  hover: boolean
}

class Pad extends React.Component<PadProps, PadState> {
  state = {
    loading: false,
    playing: false,
    hover: false
  }

  handleClick = () => this.props.onClick(this.props.index)
  handleLoadClick = (e: React.MouseEvent) => {
    e.stopPropagation()
    this.setState({ loading: true })
    this.props.onClickLoad(this.props.index, () => this.setState({ loading: false }))
  }
  handleRemoveClick = (e: React.MouseEvent) => {
    e.stopPropagation()
    this.props.onClickRemove(this.props.index)
  }
  handlePlayClick = (e: React.MouseEvent) => {
    e.stopPropagation();
    this.setState({ playing: true })
    this.props.onClickPlay(this.props.index, () => this.setState({ playing: false }))
  }
  handleStopClick = (e: React.MouseEvent) => {
    e.stopPropagation();
    this.setState({ playing: false })
    this.props.onClickStop(this.props.index)
  }

  toggleHover = () => this.setState({ hover: !this.state.hover })

  render() {
    return (
      <Wrapper active={this.props.active} loaded={!!this.props.waveform}
          onClick={this.handleClick}
          onMouseEnter={this.toggleHover}
          onMouseLeave={this.toggleHover}>
        <PadAction hidden={!this.state.hover || !this.props.waveform}
            onClick={this.handleRemoveClick}>X</PadAction>
        <PadAction middle hidden={!this.state.hover}
            onClick={this.handleLoadClick}>+</PadAction>
        <PadAction bottom hidden={!this.state.hover || !this.props.waveform || this.state.playing}
            onClick={this.handlePlayClick}>&#9658;</PadAction>
        <PadAction bottom hidden={!this.state.playing || !this.props.waveform}
            onClick={this.handleStopClick}>&#9632;</PadAction>
      </Wrapper>
    )
  }
}

export default Pad
