import React from 'react'
import styled from 'styled-components'

import Pad, { Wrapper as PadWrapper } from './Pad'

export const Wrapper = styled.div`
  border: 1px solid black;
  padding: 5px;
  min-width: 280px;
  max-width: 360px;
`

const BankLabel = styled.span`
  margin-left: 5px;
  margin-right: 5px;
`

const PadList = styled.div`
  min-height: 160px;

  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-around;

  ${PadWrapper} {
    flex-shrink: 0;
    flex-grow: 0.1;
    flex-basis: 21%;
  }
`

interface BankProps {
  label: string
  children: React.ReactNode
}

const Bank: React.FC<BankProps> = (props) => (
  <Wrapper>
    <BankLabel>{props.label}</BankLabel>
    <PadList>
      {props.children}
    </PadList>
  </Wrapper>
)

export default Bank
