import React from 'react'
import styled from 'styled-components'

import Bank, { Wrapper as BankWrapper } from './Bank'

const Wrapper = styled.div`
  width: 100%;
  max-width: 1750px;

  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;

  --gap: 12px;
  margin: calc(-1 * var(--gap)) 0 0 calc(-1 * var(--gap));
  width: calc(100% + var(--gap));

  ${BankWrapper} {
    flex-shrink: 0;
    flex-grow: 0.1;
    flex-basis: 18%;

    margin: var(--gap) 0 0 var(--gap);
  }
`

const BankList: React.FC = (props) => (
  <Wrapper>
    {props.children}
  </Wrapper>
)

export default BankList
