import styled from 'styled-components'

export const Colors = {
  CYAN: 'cyan',
  DARK_CYAN: '#00EEEE',
  YELLOW: 'yellow',
  DARK_YELLOW: '#EEEE00',
  MAGENTA: 'magenta',
  DARK_MAGENTA: '#EE00EE',
  GRAY: '#EEEEEE'
}

export const Container = styled.div`
  padding: 10px;
  margin: 0;
  width: 100%;
  height: 100%;
`

export const Row = styled.div`
  margin: 10px 0;
  width: 100%;
`
