import React from 'react'
import styled from 'styled-components'

import { Colors } from './style'

const padActionR = 8
const PadAction = styled.div<{ hidden?: boolean, middle?: boolean, bottom?: boolean }>`
  background-color: white;
  border: 1px solid black;
  border-radius: 50%;
  cursor: pointer;
  display: ${props => props.hidden ? 'none' : 'block'};
  font-size: 12px;
  text-align: center;
  height: ${padActionR*2}px;
  width: ${padActionR*2}px;
  position: absolute;
  ${props => 
    props.middle ? `top: calc(50% - ${padActionR}px); left: calc(50% - ${padActionR}px);`
                 : props.bottom ? 'bottom: -5px; right: -5px;'
                                : 'top: -5px; right: -5px;'
  }

  &:hover {
    background-color: ${Colors.GRAY};
  }
`

PadAction.defaultProps = {
  hidden: false,
  middle: false,
  bottom: false
}

export default PadAction
