import React from 'react'
import styled from 'styled-components'

import { Colors } from './style'

const _Button = styled.button<{ backgroundColor?: string, hoverColor?: string }>`
  background-color: ${props => props.backgroundColor || 'transparent'};
  border: 1px solid black;
  cursor: pointer;
  margin-right: 10px;
  min-width: 110px;
  padding: 10px 20px;
  text-transform: uppercase;

  &:hover {
    background-color: ${props => props.hoverColor || Colors.GRAY};
  }

  &[disabled] {
    background-color: ${Colors.GRAY};
    cursor: not-allowed;
  }
`

interface ButtonProps {
  primary?: boolean
  warning?: boolean
  danger?: boolean
  disabled?: boolean
  children: React.ReactNode
}

const Button: React.FC<ButtonProps & React.HTMLProps<HTMLButtonElement>> = (props) => {
  let backgroundColor = 'transparent'
  let hoverColor = Colors.GRAY

  if (props.primary) {
    backgroundColor = Colors.CYAN
    hoverColor = Colors.DARK_CYAN
  } else if (props.warning) {
    backgroundColor = Colors.YELLOW
    hoverColor = Colors.DARK_YELLOW
  } else if (props.danger) {
    backgroundColor = Colors.MAGENTA
    hoverColor = Colors.DARK_MAGENTA
  }

  return (
    <_Button onClick={props.onClick} disabled={props.disabled}
        backgroundColor={backgroundColor} 
        hoverColor={hoverColor}>
      {props.children}
    </_Button>
  )
}

Button.defaultProps = {
  primary: false,
  warning: false,
  danger: false,
  disabled: false,
}

export default Button
