import React from 'react'
import styled from 'styled-components'

import Button from './Button'
import { Row } from './style'

const Wrapper = styled.div`
  padding: 10px;
  text-align: center;
`

export interface DriveControlsProps {
  drive: string
  onClickSelect: () => void
  onClickImport: () => void
  onClickExport: () => void
}

const DriveControls: React.FC<DriveControlsProps> = (props) => (
  <Wrapper>
    <Row>
      DRIVE:&nbsp;
      <Button onClick={props.onClickSelect} primary={!props.drive}>{props.drive || 'Browse'}</Button>
    </Row>
    <Row>
      <Button onClick={props.onClickImport} primary disabled={!props.drive}>Import</Button>
      <Button onClick={props.onClickExport} warning
              disabled={!props.drive}>
        Export
      </Button>
    </Row>
  </Wrapper>
)

export default DriveControls
