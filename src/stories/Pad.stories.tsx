import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import Pad from '../components/Pad'

export default {
  title: 'BankLooter/Pad',
  component: Pad,
} as Meta

const Template: Story = (args) => <Pad {...args} />

export const Empty = Template.bind({})
Empty.args = {
  index: 1
}
