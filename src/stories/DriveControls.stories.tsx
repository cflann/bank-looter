import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import DriveControls from '../components/DriveControls'

export default {
  title: 'BankLooter/DriveControls',
  component: DriveControls,
} as Meta

export const NoDrive = (args) => <DriveControls />
export const DriveSelected = (args) => <DriveControls drive={'/media/user/SP-404SX'} />
