import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import Bank from '../components/Bank'
import { Empty as EmptyPad } from './Pad.stories'

export default {
  title: 'BankLooter/Bank',
  component: Bank,
} as Meta

export const Empty = (args) => <Bank label='A'></Bank>

export const WithPads = (args) => (
  <Bank label={args.label || 'A'}>
    <EmptyPad />
    <EmptyPad />
    <EmptyPad />
    <EmptyPad />
    <EmptyPad />
    <EmptyPad />
    <EmptyPad />
    <EmptyPad />
    <EmptyPad />
    <EmptyPad />
    <EmptyPad />
    <EmptyPad />
  </Bank>
)
