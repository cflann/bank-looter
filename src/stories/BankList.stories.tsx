import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import BankList from '../components/BankList'
import { WithPads as Bank } from './Bank.stories'

export default {
  title: 'BankLooter/BankList',
  component: BankList,
} as Meta

export const FullList = (args) => (
  <BankList>
    <Bank label='A' />
    <Bank label='B' />
    <Bank label='C' />
    <Bank label='D' />
    <Bank label='E' />
    <Bank label='F' />
    <Bank label='G' />
    <Bank label='H' />
    <Bank label='I' />
    <Bank label='J' />
  </BankList>
)
