import React from 'react'
import { Story, Meta } from '@storybook/react/types-6-0'

import { Container } from '../components/style'

import { 
  NoDrive,
  DriveSelected 
} from '../stories/DriveControls.stories'
import { FullList as BankList } from '../stories/BankList.stories'

export default {
  title: 'BankLooter/App',
} as Meta

export const Empty = (args) => (
  <Container>
    <NoDrive />
    <BankList />
  </Container>
)

export const ActiveDrive = (args) => (
  <Container>
    <DriveSelected />
    <BankList />
  </Container>
)
