import { Action } from '@reduxjs/toolkit'
import { ThunkAction } from 'redux-thunk'

import { Sample } from 'sp404-tools'

interface AppState {
  app: {
    activeIndex: number
    exporting: boolean
  }
  drive: {
    path: string
  }
  samples: Sample[]
}

export type AppThunk = ThunkAction<void, AppState, unknown, Action<string>>

export default AppState
