import React from 'react'
import { Dispatch } from 'redux'
import { connect, useDispatch, useSelector } from 'react-redux'

import { setActiveIndex } from '../reducers/appSlice'
import { clearSample, loadSample, selectWaveFile } from '../reducers/samplesSlice'
import AppState from '../store'
import Pad, { PadProps } from '../components/Pad'

const PadManager: React.FC<Partial<PadProps>> = (props) => {
  const dispatch = useDispatch()
  const { activeIndex, sample } = useSelector(
    (state: AppState) => ({
      activeIndex: state.app.activeIndex,
      sample: state.samples[props.index]
    })
  )

  return (
    <Pad index={props.index}
      active={props.index === activeIndex}
      waveform={sample.sourcePath || ''}
      onClick={(index: number) => dispatch(setActiveIndex(index))}
      onClickRemove={(index: number) => dispatch(clearSample({ index }))}
      onClickLoad={(index: number, done: () => void) => dispatch(selectWaveFile(index))}
      onClickPlay={(index: number) => undefined}
      onClickStop={(index: number) => undefined} />
  )
}

export default PadManager
