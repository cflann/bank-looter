import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { selectDrive, importDrive, exportDrive } from '../reducers/driveSlice'
import AppState from '../store'
import DriveControls, { DriveControlsProps } from '../components/DriveControls'

const DriveManager: React.FC = (props) => {
  const dispatch = useDispatch()
  const { path: drive } = useSelector(
    (state: AppState) => state.drive
  )

  return (
    <DriveControls drive={drive}
      onClickSelect={() => dispatch(selectDrive())}
      onClickImport={() => dispatch(importDrive())}
      onClickExport={() => dispatch(exportDrive())} />
  )
}

export default DriveManager
