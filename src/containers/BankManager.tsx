import React from 'react'
import { connect } from 'react-redux'
import range from 'lodash/range'

import { clearSample, loadSample } from '../reducers/samplesSlice'
import Bank from '../components/Bank'
import BankList from '../components/BankList'
import PadManager from './PadManager'

const BankManager: React.FC = (props) => (
  <BankList>
    {'ABCDEFGHIJ'.split('').map((label, bankIndex) => (
      <Bank key={label} label={label}>
        {range(12).map((padIndex: number) => (
          <PadManager key={padIndex} index={bankIndex*12 + padIndex} />
        ))}
      </Bank>
    ))}
  </BankList>
)

export default BankManager
