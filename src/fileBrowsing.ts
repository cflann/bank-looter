const { dialog } = require('electron')

export const selectDrive = () => {
  return dialog.showOpenDialog({
    title: 'SD Card Import',
    defaultPath: '/',
    buttonLabel: 'Import',
    properties: ['openDirectory']
  })
}

export const selectWav = () => {
  return dialog.showOpenDialog({
    title: 'Select Sample',
    buttonLabel: 'Open',
    filters: [
      { name: 'Audio', extensions: ['wav'] }
    ]
  })
}
